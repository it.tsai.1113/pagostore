import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'pm-pago-wallet-download',
  templateUrl: './pago-wallet-download.component.html',
  styleUrls: ['./pago-wallet-download.component.scss'],
})
export class PagoWalletDownloadComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openDialog(): void {
    this.dialog.open(PagoWalletDownloadDialog);
  }
}

@Component({
  templateUrl: 'pago-wallet-download.dialog.html',
})
export class PagoWalletDownloadDialog {}
