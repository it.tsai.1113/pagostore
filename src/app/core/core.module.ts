import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  PagoWalletDownloadComponent,
  PagoWalletDownloadDialog,
} from './pago-wallet-download/pago-wallet-download.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import {
  OrderSearchComponent,
  OrderSearchDialog,
} from './order-search/order-search.component';

@NgModule({
  declarations: [
    PagoWalletDownloadComponent,
    PagoWalletDownloadDialog,
    OrderSearchComponent,
    OrderSearchDialog,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  exports: [PagoWalletDownloadComponent, OrderSearchComponent],
})
export class CoreModule {}
