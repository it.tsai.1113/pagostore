import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pm-order-search',
  templateUrl: './order-search.component.html',
  styleUrls: ['./order-search.component.scss'],
})
export class OrderSearchComponent implements OnInit, OnDestroy {
  dialogSub: Subscription;

  constructor(public dialog: MatDialog, private router: Router) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.dialogSub.closed) {
      return;
    }

    this.dialogSub.unsubscribe();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(OrderSearchDialog, {
      autoFocus: true,
    });

    this.dialogSub = dialogRef.afterClosed().subscribe({
      next: (data) => {
        if (!!data?.orderId) {
          this.router.navigate(['/orders', data.orderId]);
        }
      },
    });
  }
}

@Component({
  templateUrl: 'order-search.dialog.html',
})
export class OrderSearchDialog {
  searchOrderForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<OrderSearchDialog>,
  ) {}

  ngOnInit(): void {
    this.searchOrderForm = this.fb.group({
      orderId: ['', [Validators.required]],
    });
  }

  getErrorMessage(): string {
    if (this.searchOrderForm.get('orderId').hasError('required')) {
      return 'purchase id can not be empty';
    }

    return '';
  }

  submit(): void {
    this.dialogRef.close(this.searchOrderForm.value);
  }

  close() {
    this.dialogRef.close();
  }
}
