import { ProductService } from './../products/product.service';
import { IOrderResolved, IOrderDetail } from './../shared/models/order';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProductVariant } from '../shared/models/product';

@Component({
  selector: 'pm-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
})
export class OrderDetailComponent implements OnInit {
  orderDetail: IOrderDetail;
  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
  ) {}

  ngOnInit(): void {
    const resolvedData: IOrderResolved =
      this.route.snapshot.data['resolvedData'];
    this.onOrderDetailRetrieved(resolvedData.order);
  }

  onOrderDetailRetrieved(orderDetail: IOrderDetail): void {
    this.orderDetail = orderDetail;

    // map item's id and variant
    this.productService.getProducts().subscribe({
      next: (val) => {
        const products = val;
        orderDetail.items = orderDetail.items.map((item) => {
          // map item's id and variant
          let variant: IProductVariant = null;
          for (const product of products) {
            variant = product.variants.find(
              (variant) => variant.id === item.id,
            );

            if (variant) break;
          }

          return {
            ...item,
            variant: variant,
          };
        });
      },
    });

    this.errorMessage = !!this.orderDetail ? '' : 'No Purchase Found';
  }
}
