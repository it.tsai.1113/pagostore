import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { OrderResolver } from './order-resolver.service';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MatCardModule } from '@angular/material/card';
import { OrderDetailComponent } from './order-detail.component';

@NgModule({
  declarations: [OrderDetailComponent],
  imports: [
    RouterModule.forChild([
      {
        path: 'orders/:id',
        component: OrderDetailComponent,
        resolve: { resolvedData: OrderResolver },
      },
    ]),
    SharedModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatDividerModule,
  ],
})
export class OrdersModule {}
