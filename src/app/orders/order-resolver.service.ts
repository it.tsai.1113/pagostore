import { IOrderResolved } from './../shared/models/order';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { OrderService } from './order.service';

@Injectable({
  providedIn: 'root',
})
export class OrderResolver implements Resolve<IOrderResolved> {
  constructor(private orderService: OrderService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<IOrderResolved> {
    const id = route.paramMap.get('id');

    return this.orderService.getById(id).pipe(
      map((order) => ({ order })),
      catchError((error) => {
        const message = `Retrieval error: ${error}`;
        console.error(message);
        return of({ order: null, error: message });
      }),
    );
  }
}
