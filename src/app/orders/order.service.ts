import { ProductService } from './../products/product.service';
import { IProduct } from './../shared/models/product';
import { IOrderDetail, IOrderDetailItem } from './../shared/models/order';
import { ServerService } from './../shared/services/server.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

interface IOrderResponse {
  purchaseId: string;
  status: string;
  items: {
    quantity: number;
    variant_id: string;
  }[];
  recipient: {
    state_code: string;
    zip: number;
    address2: string;
    address1: string;
    city: string;
    phone: string;
    email: string;
    country_code: string;
  };
  shipping_type: string;
}

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private urls: {
    orders: string;
  };

  filters = [];

  constructor(private http: HttpClient, private serverService: ServerService) {
    this.urls = {
      orders: this.serverService.url('/purchase'),
      // orders: 'api/purchase',
    };
  }

  getById(id: string): Observable<IOrderDetail> {
    return this.http.get<IOrderResponse>(`${this.urls.orders}/${id}`).pipe(
      tap((data) => console.log('Get by id: ' + JSON.stringify(data))),
      map((result) => {
        let res: IOrderDetail = this.mapResponseToCanonical(result);
        return res;
      }),
      catchError(this.serverService.handleError),
    );
  }

  private mapResponseToCanonical(value: IOrderResponse): IOrderDetail {
    const items: IOrderDetailItem[] = value.items.map((item) => {
      return {
        id: item.variant_id,
        quantity: item.quantity,
      };
    });
    const recipient = value.recipient;

    return {
      purchaseId: value.purchaseId,
      status: value.status,
      items: items,
      recipient: {
        address1: recipient.address1,
        address2: recipient.address2,
        city: recipient.city,
        countryCode: recipient.country_code,
        email: recipient.email,
        phone: recipient.phone,
        stateCode: recipient.state_code,
        zip: recipient.zip,
      },
      shippingType: value.shipping_type,
    };
  }
}
