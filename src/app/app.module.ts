import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ProductModule } from './products/product.module';
import { CartComponent } from './cart/cart.component';
import { CartModule } from './cart/cart.module';
import { CheckoutComponent } from './checkout/checkout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { OrdersModule } from './orders/orders.module';

@NgModule({
  declarations: [AppComponent],
  providers: [{ provide: APP_BASE_HREF, useValue: 'web' }],
  imports: [
    BrowserModule,
    CoreModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'products', component: ProductModule },
      { path: 'cart', component: CartComponent },
      { path: 'checkout', component: CheckoutComponent },
      { path: '', redirectTo: 'products', pathMatch: 'full' },
      { path: '**', redirectTo: 'products', pathMatch: 'full' },
    ]),
    FlexLayoutModule,
    ProductModule,
    CartModule,
    OrdersModule,

    // Angular-Material
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatBadgeModule,
    MatButtonModule,
  ],
  bootstrap: [AppComponent],
  entryComponents: [CheckoutComponent],
})
export class AppModule {}
