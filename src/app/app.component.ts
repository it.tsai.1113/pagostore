import { Component } from '@angular/core';
import { CartService } from '../app/cart/cart.service';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private cartService: CartService) {}
  pageTitle = 'Pago Store';

  get isCartUpdated(): number {
    return this.cartService.orderItems.length
      ? this.cartService.orderItems.length
      : 0;
  }
}
