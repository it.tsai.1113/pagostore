import { IProductVariant } from './../shared/models/product';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { IProduct, IProductResolved } from '../shared/models/product';
import { IOrderedProduct } from '../shared/models/order';
import { CartService } from '../cart/cart.service';
import { SnackbarService } from '../shared/services/snackbar.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  pageTitle = 'Product Detail';
  product: IProduct;
  variant: IProductVariant = null;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private cartService: CartService,
    private _snackBar: MatSnackBar,
    private snackbarService: SnackbarService,
  ) {}

  ngOnInit(): void {
    const resolvedData: IProductResolved =
      this.route.snapshot.data['resolvedData'];
    this.onProductRetrieved(resolvedData.product);
  }

  onProductRetrieved(product: IProduct): void {
    this.product = product;

    if (this.product) {
      this.pageTitle = this.product.title;
    } else {
      this.pageTitle = 'No product found';
    }
  }

  onBack(): void {
    this.location.back();
  }

  variantChanged(variant: IProductVariant): void {
    this.variant = variant;
  }

  addToCart(order: IOrderedProduct): void {
    this.cartService.addOrderItem(order);
    this.snackbarService.openSnackBar(
      this._snackBar,
      `${order.variant.title} added to cart!`,
    );
  }
}
