import { Subscription } from 'rxjs';
import { IProductVariant } from './../shared/models/product';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IOrderedProduct } from '../shared/models/order';
import { IProduct } from '../shared/models/product';

interface IOptions {
  variants: IProductVariant[];
}

@Component({
  selector: 'pm-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.scss'],
})
export class AddToCartComponent implements OnInit, OnDestroy {
  @Input() product: IProduct = null;

  @Output() addToCartClicked: EventEmitter<IOrderedProduct> =
    new EventEmitter<IOrderedProduct>();

  @Output() variantChanged: EventEmitter<IProductVariant> =
    new EventEmitter<IProductVariant>();

  orderForm: FormGroup;

  variantChange$: Subscription;

  // TODO: will refactor after api is ready
  options: IOptions = {
    variants: [],
  };

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.options.variants = this.product.variants;
    this.orderForm = this.fb.group({
      variant: [
        {
          value: null
        },
        [Validators.required],
      ],
      quantity: [1, [Validators.required, Validators.min(1)]],
    });

    this.variantChange$ = this.orderForm
      .get('variant')
      .valueChanges.subscribe((val) => {
        this.variantChanged.emit(val);
      });

    setTimeout(() => {
      // default set to first option
      this.orderForm.patchValue({
        variant: this.options.variants[0],
      });
    }, 0);
  }

  ngOnDestroy(): void {
    this.variantChange$.unsubscribe();
  }

  getErrorMessage(): string {
    if (this.orderForm.get('quantity').hasError('required')) {
      return 'quantity can not be empty';
    }
    if (this.orderForm.get('quantity').hasError('min')) {
      return 'quantity must be > 0';
    }

    return '';
  }

  addToCart(): void {
    this.addToCartClicked.emit({
      ...this.product,
      ...this.orderForm.value,
    });
  }
}
