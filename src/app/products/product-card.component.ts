import { IOrderedProduct } from './../shared/models/order';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { IProduct, IProductVariant } from '../shared/models/product';

@Component({
  selector: 'pm-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: IProduct = null;
  @Input() isSelected: boolean = false;

  @Output() cardClicked: EventEmitter<string> = new EventEmitter<string>();
  @Output() addToCartClicked: EventEmitter<IOrderedProduct> =
    new EventEmitter<IOrderedProduct>();

  variant: IProductVariant = null;

  constructor() {}

  ngOnInit(): void {}

  onCardClick(): void {
    this.cardClicked.emit(this.product.id.toString());
  }

  addToCart(order: IOrderedProduct): void {
    this.addToCartClicked.emit(order);
  }

  variantChanged(variant: IProductVariant): void {
    this.variant = variant;
  }
}
