import { ServerService } from './../shared/services/server.service';
import { IProduct, IProductVariant } from './../shared/models/product';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

interface IProductResponseWrap {
  sync_product: {
    id: number;
    name: string;
  };
  sync_variants: IProductResponse[];
}

interface IProductResponse {
  id: number;
  external_id: string; // for sending purchase used
  warehouse_product_variant_id: string;
  name: string;
  retail_price: string;
  files: {
    thumbnail_url: string;
    preview_url: string;
  }[];
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private urls: {
    product: string;
    constants: string;
  };

  filters = [];

  constructor(private http: HttpClient, private serverService: ServerService) {
    this.urls = {
      product: this.serverService.url('/products'),
      // product: 'api/products/products.json',
      constants: 'api/constants/constants.json',
    };
  }

  // TODO: will define interface when api is ready
  getConstants(): Observable<any> {
    return this.http.get<any>(this.urls.constants).pipe(
      tap((data) => console.log('All: ' + JSON.stringify(data))),
      catchError(this.serverService.handleError),
    );
  }

  getProducts(): Observable<IProduct[]> {
    return this.http.get<IProductResponseWrap[]>(this.urls.product).pipe(
      tap((data) => console.log('All: ' + JSON.stringify(data))),
      map((result) => {
        let res: IProduct[] = result.map((rawProduct) => {
          let product: IProduct = {
            id: rawProduct.sync_product.id,
            title: rawProduct.sync_product.name,
            variants: [],
          };
          rawProduct.sync_variants.forEach((rawVariant) => {
            product.variants.push(this.mapResponseToCanonical(rawVariant));
          });

          return product;
        });

        return res;
      }),
      catchError(this.serverService.handleError),
    );
  }

  getProduct(id: number): Observable<IProduct | undefined> {
    return this.getProducts().pipe(
      map((products: IProduct[]) => products.find((p) => p.id === id)),
    );
  }

  private mapResponseToCanonical(value: IProductResponse): IProductVariant {
    const productNames = value.name.split(' - ');
    return {
      id: value.id.toString(),
      externalId: value.external_id,
      warehouseProductVariantId: value.warehouse_product_variant_id,
      title: productNames[0],
      variant: productNames[1] ?? productNames[0],
      image: value?.files[1]?.preview_url,
      thumbnail: value?.files[1]?.thumbnail_url,
      price: +value.retail_price,
    };
  }
}
