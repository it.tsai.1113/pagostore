import { SnackbarService } from './../shared/services/snackbar.service';
import { ErrorMessageComponent } from './../shared/error-message.snackbar';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { IProduct } from '../shared/models/product';
import { IOrderedProduct } from './../shared/models/order';
import { ProductService } from './product.service';
import { CartService } from '../cart/cart.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss', '../app.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: IProduct[] = [];

  _listFlex: string = '1 1 58.3333%';
  get listFlex(): string {
    return this._listFlex;
  }
  set listFlex(value: string) {
    this._listFlex = value;
  }

  constructor(
    private router: Router,
    private _snackBar: MatSnackBar,
    private productService: ProductService,
    private cartService: CartService,
    private snackbarService: SnackbarService,
  ) {}

  ngOnInit(): void {
    this.productService.getProducts().subscribe({
      next: (products) => {
        this.products = products;
      },
      error: (error) => {
        this.snackbarService.openSnackBar<ErrorMessageComponent, string>(
          this._snackBar,
          ErrorMessageComponent,
          {
            data: {
              data: error,
              isShowClose: true,
              isShowReload: true,
            },
            duration: -1,
          },
        );
      },
    });
  }

  onCardClick(id: string) {
    this.router.navigate(['/products', id]);
  }

  addToCart(order: IOrderedProduct): void {
    this.cartService.addOrderItem(order);
    this.snackbarService.openSnackBar(
      this._snackBar,
      `${order.variant.title} added to cart!`,
    );
  }
}
