import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup} from '@angular/forms';
import { IOrder, IOrderedProduct } from './../shared/models/order';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { CheckoutStepper } from './../shared/enum';

@Component({
  selector: 'pm-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})

export class CheckoutComponent implements OnInit {
  checkoutStepper = CheckoutStepper;
  constants: any = {};
  pagoCheckout: FormGroup;
  isFormValidated: boolean = false;
  order: IOrder;
  message: string = '';
  currentStep: string = CheckoutStepper.INFORMATION;
  orderedItems: IOrderedProduct[];

  @Input() checkoutTotal: string;

  @Output() checkoutFinished = new EventEmitter<boolean>();
  isLoading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<CheckoutComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.checkoutTotal = data.checkoutTotal;
    this.orderedItems = data.orderedProducts;
  }

  ngOnInit(): void {}

  getPurchase(values: any): void {
    this.order = values.order;
  }

  enableNextStep(values: any): void {
    this.isFormValidated = values.isFormValidated;
    this.pagoCheckout = values.completedForm;
  }

  selectionChange(event: StepperSelectionEvent) {
    this.currentStep = event.selectedStep.label;
  }

  closeDialog() {
    this.dialogRef.close(this.currentStep === CheckoutStepper.CONFIRMATION);
  }
}
