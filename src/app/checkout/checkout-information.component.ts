import { Component, OnInit, Input, Output, EventEmitter, OnDestroy,} from '@angular/core';
import { Validators,FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CheckoutService } from './checkout.service';
import { IOrderDetailRecipient } from './../shared/models/order';
import { ICountry, IState } from './../shared/models/country';
import { MatSelectChange } from '@angular/material/select';
import { Subscription } from 'rxjs';
import { SnackbarService } from '../shared/services/snackbar.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorMessageComponent } from '../shared/error-message.snackbar';

interface NextStepEventEmitter {
  isFormValidated: boolean;
  completedForm: FormGroup;
}

@Component({
  selector: 'pm-checkout-information',
  templateUrl: './checkout-information.component.html',
  styleUrls: [
    './checkout-information.component.scss',
    './checkout.component.scss',
  ],
})
export class CheckoutInformationComponent implements OnInit, OnDestroy {
  constants: any = {};
  states: IState[] = [];
  countries: ICountry[] = [];
  countriesWithTaxNumber: string[] = ['Brazil'];
  pagoStringPattern: RegExp = /^[A-Za-z0-9._%+-]+\$pagoservices.com$/;
  closeResult = '';
  pagoCheckout: FormGroup;
  pagoCheckoutSub: Subscription;
  previousFormValidStatus: boolean = false;

  @Input()
  checkoutTotal: string;
  @Output()
  nextStepEventEmitter = new EventEmitter<NextStepEventEmitter>();
  isLoading: boolean = false;

  constructor(
    private checkoutService: CheckoutService,
    private _snackBar: MatSnackBar,
    private snackbarService: SnackbarService,
    private fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.checkoutService.getConstants().subscribe((constants) => {
      this.constants = constants;
    });
    this.checkoutService.getCountries().subscribe({
      next: (countries) => {
        this.countries = countries;
        let index = this.countries.findIndex(
          (country) => country.name === 'United States',
        );

        // no united state -> default first option
        if (index < 0) {
          index = 0;
        }

        this.states = this.countries[index].states;
        this.pagoCheckout.controls['countryName'].setValue(
          this.countries[index].name,
        );
        this.pagoCheckout.controls['countryCode'].setValue(
          this.countries[index].code,
        );

        if (!!this.states.length) {
          this.pagoCheckout.controls['stateName'].setValue(this.states[0].name);
          this.pagoCheckout.controls['stateCode'].setValue(this.states[0].code);
          this.executeFormGroupFn('stateName',['enable']);
        }
      },
      error: (error) => {
        this.openSnackBar(error, true);
      },
    });

    this.pagoCheckout = this.initForm();
    setTimeout(() => {
      this.pagoCheckoutSub = this.pagoCheckout.valueChanges.subscribe({
        next: (value) => {
            if (this.previousFormValidStatus !== this.pagoCheckout.valid) {
            this.previousFormValidStatus = this.pagoCheckout.valid;
            this.enableNextStep(
              this.pagoCheckout.valid,
              this.pagoCheckout.valid ? this.pagoCheckout : null,
            );
          }
        },
        error: (error) => {
          this.openSnackBar(error, true);
        }
      });
    }, 0);
  }

  ngOnDestroy(): void {
    this.pagoCheckoutSub.unsubscribe();
  }

  setValues(event: MatSelectChange) {
    const index = this.countries.findIndex(
      (country) => country.name === event?.value,
    );
    if (index !== -1) this.setCode('countryCode', this.countries[index].code);

    this.setStates(event);
  }

  initForm(): FormGroup {
    return this.fb.group({
      pagoString: new FormControl({ value: '', disabled: this.isLoading }, [
          Validators.required,
          Validators.pattern(this.pagoStringPattern),
        ]),
      email: new FormControl({ value: '', disabled: this.isLoading }, [
        Validators.required,
        Validators.email,
      ]),
      phone: new FormControl({ value: '', disabled: this.isLoading }, [
        Validators.required,
        Validators.pattern('[- +()0-9]{6,}'),
      ]),
      address1: new FormControl(
        { value: '', disabled: this.isLoading },
        Validators.required,
      ),
      address2: new FormControl({ value: '', disabled: this.isLoading }),
      city: new FormControl(
        { value: '', disabled: this.isLoading },
        Validators.required,
      ),
      stateName: new FormControl(
        { value: '', disabled: !this.states.length },
        Validators.required,
      ),
      stateCode: new FormControl({
        value: '',
        disabled: true,
      }),
      zip: new FormControl(
        { value: '', disabled: this.isLoading },
        Validators.required,
      ),
      countryName: new FormControl(
        { value: '', disabled: this.isLoading },
        Validators.required,
      ),
      countryCode: new FormControl({ value: '', disabled: true }),
    });
  }

  getErrorMessage(fieldName: string): string {
    if (this.pagoCheckout.get(fieldName).hasError('required')) {
      return 'You must enter a value';
    }

    return this.pagoCheckout.get(fieldName).hasError('email')
      ? 'Not a valid email'
      : this.pagoCheckout.get(fieldName).hasError('pattern')
      ? 'Not a valid format'
      : '';
  }

  setStateCode(event: MatSelectChange) {
    const index = this.states.findIndex((state) => state.name === event.value);
    if (index !== -1) this.setCode('stateCode', this.states[index].code);
  }

  setStates(event: MatSelectChange) {
    const index = this.countries.findIndex(
      (country) => country.name === event?.value && country.states,
    );
    if (index !== -1) {
      this.states = this.countries[index].states;
      this.pagoCheckout.get('stateName').setValidators(Validators.required);
      this.executeFormGroupFn('stateName',['enable']);
    } else {
      this.states = [];
      this.executeFormGroupFn('stateName',['reset', 'disable','clearValidators']);
    }
    this.executeFormGroupFn('stateCode',['reset']);
  }

  setTaxNumber(index: number) {
    if (
      index !== -1 &&
      this.countriesWithTaxNumber.includes(this.countries[index].name)
    ) {
      this.pagoCheckout.get('taxNumber').setValidators(Validators.required);
    } else {
      this.executeFormGroupFn('taxNumber',['reset', 'clearValidators']);
    }
    this.executeFormGroupFn('taxNumber',['enable']);
  }

  setCode(name: string, value: string) {
    this.pagoCheckout.controls[name].setValue(value);
  }

  extractAddress(): IOrderDetailRecipient {
    return {
      address1: this.pagoCheckout.controls.address1.value,
      address2: this.pagoCheckout.controls.address2.value,
      city: this.pagoCheckout.controls.city.value,
      stateCode: this.pagoCheckout.controls.stateCode?.value,
      countryCode: this.pagoCheckout.controls.countryCode.value,
      zip: this.pagoCheckout.controls.zip.value,
      phone: this.pagoCheckout.controls.phone.value,
      email: this.pagoCheckout.controls.email.value,
    };
  }

  enableNextStep(isValid: boolean, form: FormGroup) {
    const eventValues: NextStepEventEmitter = {
      isFormValidated: isValid,
      completedForm: form,
    };
    this.nextStepEventEmitter.emit(eventValues);
  }

  openSnackBar(error: any, isShowClose: boolean):void {
    this.snackbarService.openSnackBar<ErrorMessageComponent, string>(
      this._snackBar,
      ErrorMessageComponent,
      {
        data: {
          data: error,
          isShowClose: isShowClose,
        },
      },
    );
  }

  executeFormGroupFn(formControlName: string, fnNames: string[]) {
    for (const fnN of fnNames) {
      eval((this.pagoCheckout.get(formControlName))[fnN]());
    }
  }
}
