import { IOrder, IOrderedCompleted } from './../shared/models/order';
import { Component, Input, OnInit } from '@angular/core';
import { CheckoutComponent } from './checkout.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CheckoutService } from './checkout.service';

@Component({
  selector: 'pm-checkout-complete',
  templateUrl: './checkout-complete.component.html',
  styleUrls: ['./checkout-complete.component.scss'],
})

export class CheckoutCompleteComponent implements OnInit {

  @Input() order: IOrder;
  response: IOrderedCompleted;

  constructor(
      public dialogRef: MatDialogRef<CheckoutComponent>,
      public dialog: MatDialog,
      private checkoutService: CheckoutService) {}


  ngOnInit(): void {
    this.checkoutService.postPurchase(this.order).subscribe({
      next: (response) => {
        this.response = { payid : response.payid,
                          purchaseId : response.purchaseId,
                          shipping_type : response.shipping_type,
                          status : response.status};
      },
      error: (error) => {
        this.checkoutService.openSnackBar(error);
      },
    });
  }

  closeDialog() {
      this.dialogRef.close(true);
  }
}
