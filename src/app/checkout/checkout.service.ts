import { ServerService } from './../shared/services/server.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { IOrderShippingRate, IOrder, IOrderPrice } from './../shared/models/order';
import { ICountry, IState } from './../shared/models/country';
import { SnackbarService } from './../shared/services/snackbar.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorMessageComponent } from './../shared/error-message.snackbar';

interface ICountriesResponse {
  code: string;
  result:  ICountryResponse[];
}

interface ICountryResponse {
  code: string;
  name: string;
  states: ICountryStateResponse[];
}

interface ICountryStateResponse {
  code: string;
  name: string;
}

interface IPurchaseResponse {
  payid: string;
  purchaseId: string;
  status: string;
  items: {
    quantity: number;
    variant_id: string;
  }[];
  recipient: {
    state_code: string;
    zip: number;
    address2: string;
    address1: string;
    city: string;
    phone: string;
    email: string;
    country_code: string;
  };
  shipping_type: string;
}

@Injectable({
  providedIn: 'root',
})
export class CheckoutService {
  private urls: {
    price: string;
    shippingRate: string;
    purchase: string;
    countries: string;
    checkout: string;
  };

  // private transaction_gateway_url = 'https://transactiongateway.test.pago.dev';

  checkoutFinished$: Subject<boolean> = new Subject();

  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private snackbarService: SnackbarService,
    private serverService: ServerService,
  ) {
    this.urls = {
      price: this.serverService.url('/price'),
      //price: 'https://reqres.in/api/posts',
      shippingRate: this.serverService.url('/shipping-rates'),
      //shippingRate: 'https://reqres.in/api/posts',
      //purchase: 'https://reqres.in/api/posts',
      purchase: this.serverService.url('/purchase'),
      //countries: 'api/countries/countries.json',
      countries: this.serverService.url('/countries'),
      checkout: 'api/checkout/checkout.json'
    };
  }

  // TODO: will define interface when api is ready
  getConstants(): Observable<any> {
    return this.http.get<any>(this.urls.checkout).pipe(
      tap((data) => console.log('')),
      catchError(this.serverService.handleError),
    );
  }

  getCountries(): Observable<ICountry[]> {
    return this.http.get<ICountriesResponse>(this.urls.countries).pipe(
      tap((data) => console.log('get countries')),
      map((result) => {
        let res: ICountry[] = result.result?.map((country) => {
        //let res: ICountry[] = result.countries?.map((country) => {

          return this.mapResponseToCanonical(country);
        });
        return res;
      }),
      catchError(this.serverService.handleError),
    );
  }

  private mapResponseToCanonical(value: ICountryResponse): ICountry {
    let states: IState[] = value.states?.map((state) => {
      return {
        code: state.code,
        name: state.name,
      };
    });

    return {
      code: value.code,
      name: value.name,
      states: states,
    };
  }

  // TODO: will define interface when api is ready
  getShipping(order: IOrder): Observable<any> {
    return this.http.post<IOrderShippingRate>(this.urls.shippingRate, order).pipe(
      tap((data) => console.log('get shippingRate')),
      catchError(this.serverService.handleError),
    );
  }

  // TODO: will define interface when api is ready
  getPrice(order: IOrder): Observable<any> {
    return this.http.post<IOrderPrice>(this.urls.price, order).pipe(
      tap((data) => console.log('get price')),
      catchError(this.serverService.handleError),
    );
  }

  // TODO: will define interface when api is ready
  postPurchase(order: IOrder) {
    return this.http.post<IPurchaseResponse>(this.urls.purchase, order).pipe(
      tap((data) => console.log('post purchse')),
      catchError(this.serverService.handleError),
    );
  }
  openSnackBar(error: any): void {
    this.snackbarService.openSnackBar<ErrorMessageComponent, string>(
      this._snackBar,
      ErrorMessageComponent,
      {
        data: {
          data: error,
          isShowClose: true,
          isShowReload: false,
        },
        duration: -1,
      },
    );
  }

  // TODO: when confirm item_ids
  getPriceReqBody(order: IOrder): any {
    return {
      item_ids: order.items,
      shipping_address: {
        address1: order.recipient.address1,
        address2: order.recipient.address2,
        city: order.recipient.city,
        state_code: order.recipient.stateCode,
        country_code: order.recipient.countryCode,
        zip: order.recipient.zip,
        phone: order.recipient.phone,
        email: order.recipient.email,
      },
    };
  }
}
