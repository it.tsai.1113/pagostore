import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IOrderDetailRecipient, IOrder, IOrderedProduct, IOrderItem, IOrderShippingRate, IOrderPrice } from '../shared/models/order';
import { CartService } from '../cart/cart.service';
import { CheckoutService } from './checkout.service';
import { MatRadioChange } from '@angular/material/radio';
import { ShippingType } from './../shared/enum';
interface PurchaseEventEmitter {
purchase: IOrder;
}

@Component({
selector: 'pm-checkout-summary',
templateUrl: './checkout-summary.component.html',
styleUrls: ['./checkout-summary.component.scss'],
})
export class CheckoutSummaryComponent implements OnInit {
constants: any = {};
totalPrice: number = 0;
orderedProducts: IOrderedProduct[] = [];
orderedItems: IOrderItem[] = [];
order: IOrder = null;
shippingRates: IOrderShippingRate[]  = [];
price: IOrderPrice = null;

shippingType: string = ShippingType.STANDARD;

@Input() pagoCheckout: FormGroup;
@Output() purchaseEventEmitter = new EventEmitter<PurchaseEventEmitter>();

isLoading: boolean = false;

constructor(
  private cartService: CartService,
  private checkoutService: CheckoutService,) { }

ngOnInit(): void {
  this.checkoutService.getConstants().subscribe({
    next: (data) => {
      this.constants = data;
    },
    error: (error) => {
        this.checkoutService.openSnackBar(error);
    },
  });
  this.orderedProducts = this.cartService.getOrderItems();
  this.orderedItems = this.orderedProducts.map(orderedProduct => {
    return { id : orderedProduct.variant.id,
              externalId : orderedProduct.variant.externalId,
              warehouseProductVariantId : orderedProduct.variant.warehouseProductVariantId,
              quantity : orderedProduct.quantity}
    })
  this.order = { payid: this.pagoCheckout.controls.pagoString.value,
                  items: this.orderedItems,
                  recipient: this.extractAddress(this.pagoCheckout),
                  shippingType: this.shippingType};


  this.checkoutService.getShipping(this.order).subscribe({
      next: (data) => {
          this.shippingRates = data;
        },
        error: (error) => {
            this.checkoutService.openSnackBar(error);
        },
    });
  this.getPrice(this.order)

  this.sendPurchaseEvent(this.order);
}

extractAddress(form: FormGroup): IOrderDetailRecipient {
    return {
        address1: form.controls.address1.value,
        address2: form.controls.address2.value,
        city: form.controls.city.value,
        stateCode: form.controls.stateCode.value,
        countryCode: form.controls.countryCode.value,
        zip: form.controls.zip.value,
        phone: form.controls.phone.value,
        email: form.controls.email.value,
    }
}

getPrice(order: IOrder) {
  this.checkoutService.getPrice(order).subscribe({
  //this.checkoutService.getFakePirce(order).subscribe({
    next: (data) => {
      this.totalPrice = data.total_price;
    },
    error: (error) => {
      this.checkoutService.openSnackBar(error);
    },
  });
}

updateTotalPrice(event: MatRadioChange):void {
  this.order.shippingType = event.value;
  this.getPrice(this.order);
  this.sendPurchaseEvent(this.order);
}

sendPurchaseEvent(order: IOrder): void {
  const eventValues: PurchaseEventEmitter = { purchase: order};
  this.purchaseEventEmitter.emit(eventValues);
}
}
