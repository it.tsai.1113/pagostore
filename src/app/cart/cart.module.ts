import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CartComponent } from './cart.component';
import { SharedModule } from '../shared/shared.module';
import { CheckoutModule } from '../checkout/checkout.module';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { CartItemComponent } from './cart-item.component';

@NgModule({
  imports: [
    RouterModule.forChild([{ path: 'cart', component: CartComponent }]),
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    SharedModule,
    CheckoutModule,
  ],
  declarations: [CartComponent, CartItemComponent],
})
export class CartModule {}
