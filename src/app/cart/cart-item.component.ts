import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IOrderedProduct } from '../shared/models/order';

@Component({
  selector: 'pm-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent implements OnInit {
  @Input() orderItem: IOrderedProduct = null;

  @Output() onRemoveClicked: EventEmitter<IOrderedProduct> =
    new EventEmitter<IOrderedProduct>();
  @Output() onPlusClicked: EventEmitter<number> = new EventEmitter<number>();
  @Output() onMinusClicked: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  onRemoveClick(product: IOrderedProduct) {
    this.onRemoveClicked.emit(product);
  }

  onPlusClick() {
    this.onPlusClicked.emit(1);
  }

  onMinusClick() {
    this.onMinusClicked.emit(-1);
  }
}
