import { IOrderedProduct } from './../shared/models/order';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  orderItems: IOrderedProduct[] = new Array();

  constructor() {}

  getOrderItems(): IOrderedProduct[] {
    return this.orderItems;
  }

  addOrderItem(orderItem: IOrderedProduct): void {
    const cloneProduct = JSON.parse(JSON.stringify(orderItem));
    const productIndex = this.orderItems.findIndex(
      (anOldItem) =>
        anOldItem.variant.id === orderItem.variant.id
    );
    if (productIndex !== -1) {
      this.orderItems[productIndex].quantity = orderItem.quantity;
      this.orderItems[productIndex].totalPrice =
        orderItem.quantity * orderItem.variant.price;
    } else {
      cloneProduct.totalPrice =  orderItem.quantity * orderItem.variant.price;
      this.orderItems.push(cloneProduct);
    }
  }

  getPrice() {
    if (this.orderItems.length === 0) {
      return 0;
    }
    return this.orderItems
      .map((product) => product.totalPrice)
      .reduce((sum, current) => sum + current);
  }

  updateQuantity(sign: number, orderItem: IOrderedProduct): IOrderedProduct[] {
    const index = this.orderItems.indexOf(orderItem);
    if (index > -1 && Number(this.orderItems[index].quantity) + sign >= 1) {
      this.orderItems[index].quantity =
        Number(this.orderItems[index].quantity) + sign;
      this.orderItems[index].totalPrice = this.orderItems[index].quantity * this.orderItems[index].variant.price;
    }
    return this.orderItems;
  }

  removeOrderItem(orderItemToRemove: IOrderedProduct): IOrderedProduct[] {
    const index = this.orderItems.indexOf(orderItemToRemove);
    if (index > -1) {
      this.orderItems.splice(index, 1);
    }

    return this.orderItems;
  }

  clearOrderItems(): void {
    this.orderItems = new Array();
  }
}
