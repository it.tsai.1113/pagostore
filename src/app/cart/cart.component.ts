import { IOrderedProduct } from './../shared/models/order';
import { Component, OnInit } from '@angular/core';

import { CartService } from './cart.service';
import { CheckoutService } from '../checkout/checkout.service';
import { CheckoutComponent } from '../checkout/checkout.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  pageTitle: string = 'Shopping Cart';
  emptyCartMsg: string = 'There is no items in your cart.'
  orderedProducts: IOrderedProduct[] = [];
  totalPrice: number = 0;

  constructor(
    private cartService: CartService,
    private checkoutService: CheckoutService,
    public dialog: MatDialog,
  ) {}

  get isCheckoutDisabled() {
    return this.orderedProducts.length === 0;
  }

  ngOnInit(): void {
    this.updateProductsAndTotalPrice();
  }

  removeOrderItem(itemToRemove: IOrderedProduct) {
    this.orderedProducts = this.cartService.removeOrderItem(itemToRemove);
    this.totalPrice = this.cartService.getPrice();
  }

  openCheckout() {
    const dialogRef = this.dialog.open(CheckoutComponent, {
      disableClose: true,
      maxWidth: '800px',
      maxHeight: '100vh',
      data: {
        checkoutTotal: this.totalPrice,
        orderedItems: this.orderedProducts,
      },
    });
    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (result !== true) {
          // error handling
          console.error(result);
          return;
        }
        this.cartService.clearOrderItems();
        this.updateProductsAndTotalPrice();

        this.checkoutService.checkoutFinished$.next(true);
      },
    });
  }

  updateQuantity(sign: number, product: IOrderedProduct): void {
    this.orderedProducts = this.cartService.updateQuantity(sign, product);
    this.totalPrice = this.cartService.getPrice();
  }

  updateProductsAndTotalPrice() {
    this.orderedProducts = this.cartService.getOrderItems();
    this.totalPrice = this.cartService.getPrice();
  }
}
