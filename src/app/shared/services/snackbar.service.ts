import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ISnackbarData } from '../models/snackbar-data';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor() {}

  openSnackBar<T, V>(
    ref: MatSnackBar,
    content: string | ComponentType<T>,
    config?: MatSnackBarConfig<ISnackbarData<V>>,
  ) {
    config = {
      horizontalPosition: 'end',
      verticalPosition: 'bottom',
      duration: 3000,
      ...config,
    };

    if (typeof content === 'string') {
      ref.open(content, '', config);
      return;
    }

    ref.openFromComponent(content, config);
  }
}
