import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { StarComponent } from './star.component';
import { FormsModule } from '@angular/forms';
import { ToastsContainerComponent } from './toasts-container.component';
import { ClickStopPropagationDirective } from './click-stop-propagation.directive';
import { ErrorMessageComponent } from './error-message.snackbar';

@NgModule({
  imports: [CommonModule, MatButtonModule],
  declarations: [
    StarComponent,
    ToastsContainerComponent,
    ClickStopPropagationDirective,
    ErrorMessageComponent,
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    StarComponent,
    ToastsContainerComponent,
    ClickStopPropagationDirective,
    ErrorMessageComponent,
  ],
})
export class SharedModule {}
