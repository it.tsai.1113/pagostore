export interface IPage<T> {
  contents: T[];
  total: number;
  page_number: number;
  page_size: number;
}
