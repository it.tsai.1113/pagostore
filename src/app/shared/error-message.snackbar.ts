import { ISnackbarData } from './models/snackbar-data';
import { Component, Inject, OnInit } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';

@Component({
  selector: 'pm-error-message',
  templateUrl: './error-message.snackbar.html',
  styleUrls: ['./error-message.snackbar.scss'],
})
export class ErrorMessageComponent implements OnInit {
  errorMessage: string;

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: ISnackbarData<string>,
    @Inject(MatSnackBarRef) public ref: MatSnackBarRef<any>,
  ) {}

  ngOnInit(): void {
    this.errorMessage = this.data.data;
  }

  reload(): void {
    window.location.reload();
  }

  close(): void {
    this.ref.dismiss();
  }
}
