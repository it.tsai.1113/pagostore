export interface ICountry {
    name: string;
    code: string;
    states: IState[];
  }

  export interface IState {
      code: string;
      name: string;
  }