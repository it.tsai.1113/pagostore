import { IProductVariant } from './product';

export interface IOrder {
  payid: string;
  shippingType: string;
  recipient: IOrderDetailRecipient;
  items: IOrderItem[];
}

export interface IOrderedProduct {
  variant: IProductVariant;
  quantity: number;
  totalPrice: number;
}

export interface IOrderItem {
  id: string;
  externalId: string;
  warehouseProductVariantId: string;
  quantity: number;
}

export interface IOrderShippingRate {
  id: string;
  name: string;
  rate: string;
  currency: string;
  minDeliveryDays: number;
  maxDeliveryDays: number;
}

export interface IOrderPrice {
  total_price: string;
  receipt: string;
  shipping_type: string;
}

export interface IOrderedCompleted {
  purchaseId: string;
  status: string;
  payid:string;
  shipping_type: string;
}

export interface IOrderDetail {
  purchaseId: string;
  status: string;
  shippingType: string;
  recipient: IOrderDetailRecipient;
  items: IOrderDetailItem[];
}

export interface IOrderDetailRecipient {
  stateCode?: string;
  zip: number;
  address1: string;
  address2: string;
  city: string;
  phone: string;
  email: string;
  countryCode: string;
}

export interface IOrderDetailItem {
  quantity: number;
  id: string;
  variant?: IProductVariant;
}

export interface IOrderResolved {
  order: IOrderDetail;
  error?: any;
}
