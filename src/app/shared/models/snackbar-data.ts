export interface ISnackbarData<T> {
  isShowReload?: boolean;
  isShowClose?: boolean;
  data: T;
}
