export interface IProduct {
  id: number;
  title: string;
  variants: IProductVariant[];
}

export interface IProductVariant {
  id: string;
  externalId: string;
  warehouseProductVariantId: string;
  title: string;
  variant: string;
  image: string;
  thumbnail: string;
  price: number;
}

export interface IProductResolved {
  product: IProduct;
  error?: any;
}
