export enum CheckoutStepper {
  INFORMATION = 'Information',
  SUMMARY = 'Summary',
  CONFIRMATION = 'Confirmation',
}
export enum ShippingType {
  STANDARD = 'STANDARD',
  PRINTFUL_FAST = 'PRINTFUL_FAST',
}
