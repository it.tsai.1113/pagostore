#!/bin/bash

aws s3 sync dist/demo-website s3://${S3_BUCKET} --acl public-read --delete
aws cloudfront create-invalidation \
    --distribution-id ${DISTRIBUTION_ID} \
    --paths /\*
